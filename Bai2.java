import java.util.Scanner;

public class Bai2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        checking(a, n);
    }

    public static void checking(int[] a, int n) {
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] != a[i+1]) {
                count++;
            }
        }
        int b = n - count - 1;
        System.out.println(b);
    }
}
